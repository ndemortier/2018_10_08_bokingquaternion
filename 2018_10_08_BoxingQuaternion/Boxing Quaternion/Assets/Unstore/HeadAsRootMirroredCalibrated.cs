﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadAsRootMirroredCalibrated : MonoBehaviour
{

    public Transform m_toLeftHandAffect;
    public Transform m_toRightHandAffect;

    public Transform m_root;
    public Transform m_head;
    public Transform m_leftHand;
    public Transform m_rightHand;


    public Quaternion m_currentRotationLeft;
    public Quaternion m_currentRotationRight;

    public Quaternion m_calibrationLeft;
    public Quaternion m_calibrationRight;

    public bool m_launchCalibrationAtStart=true;

    private void Start()
    {
        LaunchCalibration(5);
    }

    void Update()
    {
        m_toLeftHandAffect.parent = m_root;


        //STUDENT NOTE: Grimbal Lock Affect 
        //Vector3 localPosFromHead = m_head.InverseTransformPoint(m_leftHand.position);
        //Vector3 localDirFromHead = m_head.InverseTransformDirection(m_leftHand.forward);
        //m_toLeftHandAffect.localPosition = localPosFromHead;
        //m_toLeftHandAffect.forward = m_root.TransformDirection( localDirFromHead);

        SetHandPosition(m_leftHand, m_toLeftHandAffect, ref m_currentRotationLeft, ref m_calibrationLeft);
        SetHandPosition(m_rightHand, m_toRightHandAffect, ref m_currentRotationRight, ref m_calibrationRight);

    }

    private void SetHandPosition(Transform handTracked, Transform handAffected,ref  Quaternion debug, ref Quaternion calibration)
    {
        Vector3 localPosFromHead = m_head.InverseTransformPoint(handTracked.position);
        Quaternion localRotFromHead = Quaternion.Inverse(m_head.rotation) * handTracked.rotation;
        //STUDENT NOTE: Commutativité du Quaternion
        // localRotFromHead =  handTracked.rotation * Quaternion.Inverse(m_head.rotation);


        localPosFromHead.x = -localPosFromHead.x;
        localPosFromHead.z = -localPosFromHead.z;
        localRotFromHead = Quaternion.Euler(0, 180f, 0) * localRotFromHead;
        debug = localRotFromHead;

        localRotFromHead = localRotFromHead * Quaternion.Inverse(calibration) ;


        handAffected.localPosition = localPosFromHead;
        handAffected.localRotation = localRotFromHead;




    }

    public void LaunchCalibration(float delay) {
        Invoke("Calibration", delay);
    }
    public void Calibration()
    {
        m_calibrationLeft = m_currentRotationLeft;
        m_calibrationRight = m_currentRotationRight;
    }

}